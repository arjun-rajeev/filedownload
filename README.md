# USAGE #


```
#!java

fn.setURISpecifiedInAnchorElement(driver.findElement(By.cssSelector("li>a[href*=Guide]")));
File f = fn.downloadFile();
```
# OR #

```
#!java

fn.setURI("<linkToFile>");
File f = fn.downloadFile();
```