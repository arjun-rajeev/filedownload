package com.cigniti.download;

import org.apache.http.client.methods.*;

/**
 * Enumerates the available options in Http Requests
 * 
 * @author Arjun.Rajeev
 */
public enum RequestMethod
{
	OPTIONS(new HttpOptions()),
	GET(new HttpGet()),
	HEAD(new HttpHead()),
	POST(new HttpPost()),
	PUT(new HttpPut()),
	DELETE(new HttpDelete()),
	TRACE(new HttpTrace());
	private final HttpRequestBase requestMethod;
	
	RequestMethod(HttpRequestBase requestMethod)
	{
		this.requestMethod = requestMethod;
	}
	
	public HttpRequestBase getRequestMethod()
	{
		return this.requestMethod;
	}
}
