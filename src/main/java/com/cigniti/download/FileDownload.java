package com.cigniti.download;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.log4j.Logger;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * FileDownloader class will provide the selenium scripts the capability of downloading file without relying on
 * supporting tools like autoIT, robot or sikuli. The main intention of this class is to allow file downloads in a
 * distributed test execution environments like BrowserStack or Saucelabs, which is not possible other wise.
 * 
 * @author Arjun.Rajeev
 */
public class FileDownload
{
	private static final Logger LOG = Logger.getLogger(FileDownload.class);
	private RemoteWebDriver driver;
	private boolean followRedirects = true;
	private boolean mimicWebDriverCookieState = true;
	private RequestMethod httpRequestMethod = RequestMethod.GET;
	private URI fileURI;
	
	public FileDownload(RemoteWebDriver driverObject)
	{
		this.driver = driverObject;
	}
	
	/**
	 * Specify if the FileDownloader class should follow redirects when trying to download a file Default: true
	 *
	 * @param followRedirects
	 * boolean
	 */
	public void followRedirectsWhenDownloading(boolean followRedirects)
	{
		this.followRedirects = followRedirects;
	}
	
	/**
	 * Mimic the cookie state of WebDriver (Defaults to true) This will enable you to access files that are only
	 * available when logged in. If set to false the connection will be made as an anonymouse user
	 *
	 * @param mimicWebDriverCookies
	 * boolean
	 */
	public void mimicWebDriverCookieState(boolean mimicWebDriverCookies)
	{
		mimicWebDriverCookieState = mimicWebDriverCookies;
	}
	
	/**
	 * Set the HTTP Request Method Default: GET
	 *
	 * @param requestType
	 * RequestMethod
	 */
	public void setHTTPRequestMethod(RequestMethod requestType)
	{
		httpRequestMethod = requestType;
	}
	
	/**
	 * Specify a URL that you want to perform an HTTP Status Check upon/Download a file from
	 *
	 * @param linkToFile
	 * String
	 * @throws MalformedURLException
	 * @throws URISyntaxException
	 */
	public void setURI(String linkToFile) throws MalformedURLException, URISyntaxException
	{
		fileURI = new URI(linkToFile);
	}
	
	/**
	 * Specify a URL that you want to perform an HTTP Status Check upon/Download a file from
	 *
	 * @param linkToFile
	 * URI
	 * @throws MalformedURLException
	 */
	public void setURI(URI linkToFile) throws MalformedURLException
	{
		fileURI = linkToFile;
	}
	
	/**
	 * Specify a URL that you want to perform an HTTP Status Check upon/Download a file from
	 *
	 * @param linkToFile
	 * URL
	 */
	public void setURI(URL linkToFile) throws URISyntaxException
	{
		fileURI = linkToFile.toURI();
	}
	
	/**
	 * Perform an HTTP Status Check upon/Download the file specified in the href attribute of a WebElement
	 *
	 * @param anchorElement
	 * Selenium WebElement
	 * @throws Exception
	 */
	public void setURISpecifiedInAnchorElement(WebElement anchorElement) throws Exception
	{
		if (anchorElement.getTagName().equals("a"))
		{
			fileURI = new URI(anchorElement.getAttribute("href"));
		}
		else
		{
			throw new Exception("You have not specified an <a> element!");
		}
	}
	
	/**
	 * Perform an HTTP Status Check upon/Download the image specified in the src attribute of a WebElement
	 *
	 * @param imageElement
	 * Selenium WebElement
	 * @throws Exception
	 */
	public void setURISpecifiedInImageElement(WebElement imageElement) throws Exception
	{
		if (imageElement.getTagName().equals("img"))
		{
			fileURI = new URI(imageElement.getAttribute("src"));
		}
		else
		{
			throw new Exception("You have not specified an <img> element!");
		}
	}
	
	/**
	 * Load in all the cookies WebDriver currently knows about so that we can mimic the browser cookie state
	 *
	 * @param seleniumCookieSet
	 * Set&lt;Cookie&gt;
	 */
	private BasicCookieStore mimicCookieState(Set<Cookie> seleniumCookieSet)
	{
		BasicCookieStore copyOfWebDriverCookieStore = new BasicCookieStore();
		for (Cookie seleniumCookie : seleniumCookieSet)
		{
			BasicClientCookie duplicateCookie = new BasicClientCookie(seleniumCookie.getName(), seleniumCookie.getValue());
			duplicateCookie.setDomain(seleniumCookie.getDomain());
			duplicateCookie.setSecure(seleniumCookie.isSecure());
			duplicateCookie.setExpiryDate(seleniumCookie.getExpiry());
			duplicateCookie.setPath(seleniumCookie.getPath());
			copyOfWebDriverCookieStore.addCookie(duplicateCookie);
		}
		return copyOfWebDriverCookieStore;
	}
	
	/**
	 * Sends the request by bypassing the java keystore certificates and provides the response
	 * 
	 * @return HttpResponse
	 * @throws IOException
	 * @throws NullPointerException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 * @throws CertificateException
	 * @throws KeyStoreException
	 */
	private HttpResponse getHTTPResponse() throws IOException, NullPointerException, NoSuchAlgorithmException, KeyManagementException, CertificateException, KeyStoreException
	{
		// ByPass security exceptions due to certificate issues (by bypassing the java keystore certificates)
		SSLContext ctx = SSLContext.getInstance("TLS");
		TrustManager[] tm = { new X509TrustManager()
		{
			public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException
			{
				// do nothing only relevant for servers
			}
			
			public X509Certificate[] getAcceptedIssuers()
			{
				// also only relevant for servers
				return null;
			}
			
			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException
			{
				// also only relevant for servers
			}
		} };
		ctx.init(null, tm, new SecureRandom());
		ctx.getSocketFactory();
		if (fileURI == null) throw new NullPointerException("No file URI specified");
		HostnameVerifier hostnameVerifier = new HostnameVerifier()
		{
			public boolean verify(String string, SSLSession ssls)
			{
				return true;
			}
		};
		HttpsURLConnection.setDefaultSSLSocketFactory(ctx.getSocketFactory());
		HttpClient client = HttpClientBuilder.create().setSslcontext(ctx).setSSLHostnameVerifier(hostnameVerifier).build();
		BasicHttpContext localContext = new BasicHttpContext();
		localContext.setAttribute(HttpClientContext.COOKIE_STORE, null);
		if (mimicWebDriverCookieState)
		{
			localContext.setAttribute(HttpClientContext.COOKIE_STORE, mimicCookieState(driver.manage().getCookies()));
		}
		HttpRequestBase requestMethod = httpRequestMethod.getRequestMethod();
		requestMethod.setURI(fileURI);
		RequestConfig config = RequestConfig.custom().setRedirectsEnabled(followRedirects).build();
		requestMethod.setConfig(config);
		return client.execute(requestMethod, localContext);
	}
	
	/**
	 * Gets the HTTP status code returned when trying to access the specified URI
	 *
	 * @return File
	 * @throws Exception
	 */
	public int getLinkHTTPStatus() throws Exception
	{
		HttpResponse fileToDownload = getHTTPResponse();
		int httpStatusCode;
		try
		{
			httpStatusCode = fileToDownload.getStatusLine().getStatusCode();
		}
		finally
		{
			fileToDownload.getEntity().getContent().close();
		}
		return httpStatusCode;
	}
	
	/**
	 * Download a file from the specified URI
	 *
	 * @return File
	 * @throws Exception
	 */
	public File downloadFile(String... fileName) throws Exception
	{
		HttpResponse fileToDownload = getHTTPResponse();
		if (!fileToDownload.containsHeader("Content-Disposition"))
		{
			if (fileName.length > 0)
			{
				fileToDownload.setHeader("Content-Disposition", "attachment;filename=" + fileName[0]);
			}
			else
			{
				fileToDownload.setHeader("Content-Disposition", "attachment;filename=defaultfile");
			}
		}
		Pattern pattern = Pattern.compile("filename=(.*?)$");
		Matcher matcher = pattern.matcher(fileToDownload.getHeaders("content-disposition")[0].toString());
		matcher.find();
		File downloadedFile = new File("C:\\" + matcher.group(1));
		try
		{
			FileUtils.copyInputStreamToFile(fileToDownload.getEntity().getContent(), downloadedFile);
		}
		finally
		{
			fileToDownload.getEntity().getContent().close();
		}
		return downloadedFile;
	}
}
